import requests
from bs4 import BeautifulSoup
from dotenv import load_dotenv
from pathlib import Path
import os
import sys
import time

load_dotenv()

url = "https://nic.itb.ac.id/manajemen-akun/pengecekan-user"
cookies = {
    os.getenv("COOKIES_KEY"): os.getenv("COOKIES_VALUE")
}
majors = {
    "101": "Matematika",
    "102": "Fisika",
    "103": "Astronomi",
    "104": "Mikrobiologi",
    "105": "Kimia",
    "106": "Biologi",
    "107": "Sains dan Teknologi Farmasi",
    "108": "Aktuaria",
    "112": "Rekayasa Hayati",
    "114": "Rekayasa Pertanian",
    "115": "Rekayasa Kehutanan",
    "116": "Farmasi Klinik dan Komunitas",
    "119": "Teknologi Pasca Panen",
    "120": "Teknik Geologi",
    "121": "Teknik Pertambangan",
    "122": "Teknik Perminyakan",
    "123": "Teknik Geofisika",
    "124": "Geofisika",
    "125": "Teknik Metalurgi",
    "128": "Meteorologi",
    "129": "Oseanografi",
    "130": "Teknik Kimia",
    "131": "Teknik Mesin",
    "132": "Teknik Elektro",
    "133": "Teknik Fisika",
    "134": "Teknik Industri",
    "135": "Teknik Informatika",
    "136": "Teknik Dirgantara",
    "137": "Teknik Material",
    "143": "Teknik Pangan",
    "144": "Manajemen Rekayasa",
    "145": "Teknik Bioenergi dan Kemurgi",
    "150": "Teknik Sipil",
    "151": "Teknik Geodesi dan Geomatika",
    "152": "Arsitektur",
    "153": "Teknik Lingkungan",
    "154": "Perencanaan Wilayah dan Kota",
    "155": "Teknik Kelautan",
    "157": "Rekayasa Infrastruktur Lingkungan",
    "158": "Teknik dan Pengelolaan Sumber Daya Air",
    "170": "Seni Rupa",
    "172": "Kriya",
    "173": "Desain Interior",
    "174": "Desain Komunikasi Visual",
    "175": "Desain Produk",
    "180": "Teknik Tenaga Listrik",
    "181": "Teknik Telekomunikasi",
    "182": "Sistem dan Teknologi Informasi",
    "183": "Teknik Biomedis",
    "190": "Manajemen",
    "192": "Kewirausahaan"
}
res = set()
count = 0

def sanitize_string(string) :
    return (string.split("<td>")[1].split("</td>")[0].strip() + ",").replace("(at)", "@").replace("(dot)", ".")

def scrape(nim) :
    global count
    data = dict(uid = nim)
    fetched = False
    while not fetched :
        try:
            page = requests.post(url, data=data, cookies=cookies, timeout=30)
            soup = BeautifulSoup(page.content, "html.parser")
            if "tidak ditemukan" in soup.text :
                print("NIM " + nim + " not found", end="")
                fetched = True
            elif "Login" in soup.text :
                print("Invalid session. Update your cookie and restart the server", end="")
                exit(1)
            else :
                count += 1
                print(str(count) + ". ", end="")
                result = soup.find(id="tabel").prettify().split(":")
                result = [st.strip().replace("\n","") for st in result]
                
                res_name = sanitize_string(result[3]).split(",")
                # Handle name input with "last_name, first_name" format
                if len(res_name) > 2 :
                    res_name = (res_name[1] + " " + res_name[0]).strip() + ","
                else :
                    res_name = res_name[0] + ","
                
                res_nim = sanitize_string(result[2]).replace(" ", "")
                # Handle student with no student major code (nim jurusan)
                if len(res_nim.split(",")) == 2 : res_nim += ","
                
                res_email_itb = sanitize_string(result[5])
                
                res_email = sanitize_string(result[6]).split(",")
                # Handle student with multiple non-itb emails
                if len(res_email) > 2 :
                    res_email = [st for st in res_email if "itb" not in st][0] + ","
                else :
                    res_email = res_email[0] + ","

                res_major = majors.get(res_nim.split(",")[1].strip()[0:3])
                res_txt = res_name + res_email_itb + res_email + res_nim
                
                # Handle student with no major
                if res_major is not None :
                    res_txt += res_major

                res.add(res_txt)
                fetched = True
                print(nim + " | " + res_name, end="")
        except (requests.ConnectionError, requests.Timeout, requests.HTTPError, requests.ConnectTimeout, requests.ReadTimeout) as e:
            print(e, end="")
            for remaining in range(10, 0, -1):
                print("Retrying in {:2d} seconds ...".format(remaining), end="") 
                sys.stdout.flush()
                time.sleep(1)

def write_to_csv(values) :
    try:
        with open("./scraper/res.csv", "w") as f :
            for value in values :
                f.write(value + "\n")
        f.close()
    except Exception as e:
        print(e)
        print("Scraping terminated")
        exit(1)

def main() :
    nim_faculties = ["160", "161", "162", "163", "164", "165", "166", "167", "168", "169", "190", "197", "198", "199"]
    nim_batches = ["{:02d}".format(i) for i in range(16, 19)]
    nim_std_codes = ["{:03d}".format(i) for i in range(1, 501)]
    for faculty in nim_faculties :
        for batch in nim_batches :
            for std_code in nim_std_codes :
                nim = faculty + batch + std_code
                scrape(nim)
                sys.stdout.flush()
    write_to_csv(res)
    print("Scraping finished")

if __name__ == "__main__":
    main()