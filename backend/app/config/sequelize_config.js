require('dotenv').config({ path: __dirname + "/../../.env" });

module.exports = {
  "development": {
    "host": process.env.PRODUCTION_DB_HOST,
    "username": process.env.PRODUCTION_DB_USER,
    "password": process.env.PRODUCTION_DB_PASSWORD,
    "database": process.env.PRODUCTION_DB_NAME,
    "dialect": "mysql"
  },
  "test": {
    "host": process.env.TEST_DB_HOST,
    "username": process.env.TEST_DB_USER,
    "password": process.env.TEST_DB_PASSWORD,
    "database": process.env.TEST_DB_NAME,
    "dialect": "mysql",
    "pool": {
      "max": 1,
      "min": 0,
      "idle": 2500000,
      "acquire": 5000000
    }
  },
  "production": {
    "use_env_variable": "DATABASE_URL",
    "dialect": "mysql",
    "migrate": "safe",
    "pool": {
      "max": 1,
      "min": 0,
      "idle": 2500000,
      "acquire": 5000000
    },
    "dialectOptions": {
      "ssl": {
        "rejectUnauthorized": false
      }
    }
  }
};