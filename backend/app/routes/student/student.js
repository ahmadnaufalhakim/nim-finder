const express = require('express');
const router = express.Router();
const studentController = require('../../controllers/student/student');

router.get("/", studentController.getStudentsByQuery);

module.exports = router;