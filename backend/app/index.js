const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const { spawn } = require('child_process');
const parse = require('csv-parse');
const cors = require('cors');

const indexRouter = require('./routes/index');
const studentRouter = require('./routes/student/student');

const db = require('./models');
const Student = require('./models').Student;

const app = express();
app.use(cors());
require('dotenv').config({ path: __dirname + "/../../.env" });

// re-sync database
db.sequelize.sync({ force: false })
  .catch((err) => {
    console.log(err);
  });

// insert values to database using python scraper
if (!fs.existsSync(__dirname + '/../scraper/res.csv') || fs.readFileSync(__dirname + '/../scraper/res.csv').length === 0 || Math.abs(Math.round(new Date() - fs.statSync(__dirname + '/../scraper/res.csv').mtime) / (1000 * 60 * 60 * 24 * 365.25)) > 1) {
  Student.destroy({
    truncate: true,
    cascade: false
  });
  const proc = spawn('python' , [__dirname + '/../scraper/scraper.py']);
  proc.stdout.on('data', (data) => {
    console.log(data.toString());
  });
  proc.stdout.on("close", () => {
    var stream = fs.createReadStream(__dirname + '/../scraper/res.csv');
    stream  
      .pipe(parse({
        delimiter: ",",
        from_line: 1
      }))
      .on("data", (row) => {
        const res_obj = {
          nama: row[0],
          email_itb: row[1],
          email: row[2],
          nim_tpb: row[3],
          nim_jurusan: row[4],
          jurusan: row[5],
          createdAt: new Date(),
          updatedAt: new Date()
        };
        Student.create(res_obj);
      });
    setTimeout(() =>
      stream.on("error", (err) => {
        console.log(err);
      }),
      60000
    );
  });
}

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// basic router
app.use("/", indexRouter);
app.use("/students", studentRouter);

// define PORT
const PORT = process.env.PORT || process.env.BACKEND_PORT;
app.listen(PORT, () => {
  console.log(`App is listening on port: ${PORT}`);
});