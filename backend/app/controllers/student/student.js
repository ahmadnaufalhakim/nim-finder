const { Op } = require('sequelize');
const Student = require('../../models').Student;

exports.getStudentsByQuery = async (req, res) => {
  try {
    query = req.query.query;
    if (query) {
      return Student.findAll({
        where: {
          [Op.or] : [{
            nama: {
              [Op.like]: `%${query}%`
            }
          }, {
            nim_tpb: {
              [Op.like]: `%${query}%`
            }
          }, {
            nim_jurusan: {
              [Op.like]: `%${query}%`
            }
          }]
        }
      }).then((result) => {
          res.status(200).send(result);
      }).catch((err) => {
          res.status(500).send(err);
      });
    } else {
      return res.status(400).send("Query required!");
    }
  } catch (err) {
    return res.status(500).send(err);
  }
};
