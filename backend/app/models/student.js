'use strict';
module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    nama: DataTypes.STRING,
    email_itb: DataTypes.STRING,
    email: DataTypes.STRING,
    nim_tpb: DataTypes.STRING,
    nim_jurusan: DataTypes.STRING,
    jurusan: DataTypes.STRING
  }, {});
  Student.associate = function(models) {
    // associations can be defined here
  };
  return Student;
};