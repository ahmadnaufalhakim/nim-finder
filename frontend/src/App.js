import React, { useState, useEffect } from 'react';
import Students from './components/Students';
import axios from 'axios';
import './App.css';
import { InputGroup, FormControl } from 'react-bootstrap';
import Pagination from 'react-js-pagination';
import HttpsRedirect from 'react-https-redirect';
require('dotenv').config();

const App = () => {
  const [students, setStudents] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [studentsPerPage] = useState(10);
  const [query, setQuery] = useState();

  /**
   * Function to change the page number.
   * @param {Integer} pageNumber The page number
   */
  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  /**
   * Function to change the query
   * @param {Event} event Event of form control
   */
  const handleInputChange = (event) => {
    setQuery(event.target.value);
  };

  /**
   * Function to change query with delay
   * @param {String} value Search query
   * @param {Integer} delay Delay of search
   */
  const useDebounce = (value, delay) => {
    const [debouncedValue, setDebouncedValue] = useState(value);
    useEffect(() => {
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);
      return () => {
        clearTimeout(handler);
      };
    }, [value, delay]);
    return debouncedValue;
  };

  const debouncedQuery = useDebounce(query, 1000);

  useEffect(() => {
    const fetchStudents = async (query) => {
      setCurrentPage(1);
      setLoading(true);
      let url;
      if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
        url = `${process.env.REACT_APP_DEVELOPMENT_BACKEND_API}/students?query=${query}`;
      } else {
        url = `${process.env.REACT_APP_PRODUCTION_BACKEND_API}/students?query=${query}`;
      }
      axios.get(
        url,
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Methods": "GET"
          }
        }
      ).then((res) => {
        setStudents(res.data);
      }).catch((err) => {
        console.log(err);
      });
      setLoading(false);
    };
    
    if (debouncedQuery) {
      fetchStudents(debouncedQuery);
    } else {
      setStudents([]);
    }
  }, [debouncedQuery]);

  return (
    <HttpsRedirect>
      <div className="container mt-5 ml-5">
        <h1 className="text-primary">NIM Finder</h1>
        <InputGroup className="mt-4">
          <FormControl
            onChange={(e) => handleInputChange(e)}
            aria-label="Default"
            aria-describedby="inputGroup-sizing-default"
            placeholder="Masukkan nama atau NIM"
          />
        </InputGroup>
        <Students
          students={students}
          loading={loading}
          currentPage={currentPage}
          studentsPerPage={studentsPerPage}
        />
        <Pagination
          itemClass="page-item"
          linkClass="page-link"
          hideDisabled
          activePage={currentPage}
          itemsCountPerPage={studentsPerPage}
          totalItemsCount={students.length}
          pageRangeDisplayed={5}
          onChange={handlePageChange}
        />
      </div>
    </HttpsRedirect>
  );
};

export default App;
