import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import Sorter from './Sorter';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

const Students = ({ students, loading, currentPage, studentsPerPage }) => {
  // Get current students
  const indexOfLastStudent = currentPage * studentsPerPage;
  const indexOfFirstStudent = indexOfLastStudent - studentsPerPage;
  const [currentStudents, setCurrentStudents] = useState(students.slice(indexOfFirstStudent, indexOfLastStudent));
  
  useEffect(() => {
    setCurrentStudents(students.slice(indexOfFirstStudent, indexOfLastStudent));
  }, [students, indexOfFirstStudent, indexOfLastStudent]);

  if (loading) {
    return <h2 className="text-secondary">Loading...</h2>
  }
  
  /**
   * Function to sort alphabetically an array of objects by some specific key.
   * Use "-" for descending sort.
   * @param {String} property Key of the object to sort.
   */
  const dynamicSort = (property) => {
    var sortOrder = 1;

    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }

    return (a, b) => {
      if (sortOrder === -1) {
        return b[property].localeCompare(a[property]);
      } else {
        return a[property].localeCompare(b[property]);
      }
    }
  };

  /**
   * Function to handle radio button onClick method.
   * @param {Event} event Event of radio button
   */
  const handleSort = (event) => {
    if (event.target.name !== undefined && event.target.name !== null) {
      const params = event.target.name.split("-");
      const property = params[1] === "asc"
        ? params[0]
        : "-".concat(params[0]);
      students.sort(dynamicSort(property));
      setCurrentStudents(students.slice(indexOfFirstStudent, indexOfLastStudent));
    }
  };

  return (
    <div>
      <Table striped bordered hover className="table responsive mt-4">
        <thead style={{ textAlign: "center" }}>
          <tr className="w-25">
            <th>#</th>
            <th>
              Nama
              <Sorter name="nama" onClick={handleSort} />
            </th>
            <th>
              Email ITB
              <Sorter name="email_itb" onClick={handleSort} />
            </th>
            <th>
              Email
              <Sorter name="email" onClick={handleSort} />
            </th>
            <th>
              NIM TPB
              <Sorter name="nim_tpb" onClick={handleSort} />
            </th>
            <th>
              NIM Jurusan
              <Sorter name="nim_jurusan" onClick={handleSort} />
            </th>
            <th>
              Jurusan
            </th>
          </tr>
        </thead>
        {currentStudents && currentStudents.length > 0 &&
          <tbody>
            {currentStudents.map((currentStudent, index) => {
              return (
                <tr key={currentStudent.id}>
                  <td>{index + ((currentPage - 1) * studentsPerPage) + 1}</td>
                  <td>{currentStudent.nama}</td>
                  <td>{currentStudent.email_itb}</td>
                  <td>{currentStudent.email}</td>
                  <td>{currentStudent.nim_tpb}</td>
                  {currentStudent.nim_jurusan === ""
                    ? <>
                        <td colSpan="2" style={{ textAlign: "center", verticalAlign: "middle" }}>
                          <FontAwesomeIcon icon={ faTimes } />
                        </td>
                      </>
                    : <>
                        <td>{currentStudent.nim_jurusan}</td>
                        <td>{currentStudent.jurusan}</td>
                      </>
                  }
                </tr>
              )
            })}
          </tbody>
        }
      </Table>
    </div>
  );
}

export default Students
