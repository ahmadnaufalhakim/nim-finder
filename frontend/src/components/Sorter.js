import React from 'react';
import { ButtonGroup, ToggleButton } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons';

const Sorter = ({ name, onClick }) => {
  return (
    <ButtonGroup toggle>
      <ToggleButton className="p-1" variant="link" type="radio" name={name.concat("-asc")} size="sm" onClick={onClick}>
        <FontAwesomeIcon icon={ faSortUp } size="xs" />
      </ToggleButton>
      <ToggleButton className="p-1" variant="link" type="radio" name={name.concat("-desc")} size="sm" onClick={onClick}>
        <FontAwesomeIcon icon={ faSortDown } size="xs" />
      </ToggleButton>
    </ButtonGroup>
  );
}

export default Sorter
