const express = require('express');
const favicon = require('express-favicon');
const path = require('path');
const port = process.env.PORT || 3001;

const app = express();

app.use(favicon(__dirname + "/build/favicon.ico"));
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, "build")));

app.get("/*", (req, res) => {
  res.redirect("/");
});
app.listen(port, () => {
  console.log(`Frontend app is listening on port: ${port}`);
});